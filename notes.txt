//----------------Connect frontend to backend----------------
// Process a fetch request to the corresponding backend API
/* 
  Syntax:
    fetch('url', {options})
    .then(res => res.json())
    .then(data => {})

  Notes:
    backend must be running
    check envlocal for port
  * To change the starting port in localhost: (untested) 
    
    modify part of package.json from:

      "start": "react-scripts start"

      for Linux and MacOS to:
      "start": "PORT=3006 react-scripts start"

      Windows to:
      "start": "set PORT=3006 && react-scripts start"

  * for cors errors, make sure that app.use(cors()); is running on backend middleware

  * if map error occurs in chrome, 
    go to (F12) Developer tools → Console -> Settings (cog icon) → untick "Selected context only".


*/
//-----------------------------------------------------------