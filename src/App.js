// To start the app type npm start on gitbash on main folder
import {useState, useEffect} from 'react';
import './App.css';
import { Container } from 'react-bootstrap/';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import AppNavbar from './components/AppNavbar.js'
import CourseView from './components/CourseView.js'
import Home from './pages/Home.js';
import Catalogue from  './pages/Products2.js'
import Products from  './pages/Products.js'
import ProductAdd from  './pages/ProductAdd.js'
import ProductEdit from  './pages/ProductEdit.js'
import Login from  './pages/Login.js'
import Cart from  './pages/Cart.js'
import UserOrders from  './pages/UserOrders.js'
import Register from  './pages/Register.js'
import Users from  './pages/Users.js'
import NotFound from  './pages/ErrorPage.js'
import Logout from  './pages/Logout.js'
import {UserProvider} from './UserContext.js';


function App() {
  
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    email: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  /*useEffect(() => {
    console.log(user);
    console.log(localStorage);
  })*/


  return (
    // userprovider to distribute the methods to all route elements
    <UserProvider value={{user, setUser, unsetUser}}>
      <>
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route exact path = "/" element = {<Home/>}/>
              <Route exact path = "/catalogue" element = {<Catalogue/>}/>
              <Route exact path = "/products" element = {<Products/>}/>
              <Route exact path = "/productAdd" element = {<ProductAdd/>}/>
              <Route exact path = "/productEdit" element = {<ProductEdit/>}/>
              <Route exact path = "/carts" element = {<Cart/>}/>
              <Route exact path = "/userOrders" element = {<UserOrders/>}/>
              <Route exact path = "/login" element = {<Login/>}/>
              <Route exact path = "/register" element = {<Register/>}/>
              <Route exact path = "/users" element = {<Users/>}/>

              <Route exact path = "/courseView/:courseId" element = {<CourseView/>}/>
              <Route exact path = "/logout" element = {<Logout/>}/>
              <Route path = "*" element={<NotFound/>} />
            </Routes>
          </Container>
        </Router>
      </>
    </UserProvider>
  );
}

export default App;

