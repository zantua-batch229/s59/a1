//import {Card, Button} from 'react-bootstrap';
import {useState, useContext} from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext.js';
import {Row, Col} from 'react-bootstrap';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

export default function ProductCard({productProps}){
	//checks to see if the data was received
	console.log(productProps);
	
	// destructuring the data to avoid notation
	const {name, description, price, stock, _id, image} = productProps;
	const {user} = useContext(UserContext);



		return(
	      <Card className="mt-3" sx={{ maxWidth: 345 }}>
		      <CardMedia className="mt-3"
		        sx={{ height: 140 }}
		        image={image}
		        title={name}
		      />
		      <CardContent>
		        <Typography gutterBottom variant="h5" component="div">
		          {name}
		        </Typography>
		        <Typography variant="body2" color="text.secondary">
		          {description}
		        </Typography>
		      </CardContent>
		      <CardActions>
{/*		        <Button size="small">Share</Button>
		        <Button size="small">Learn More</Button>*/}
				{(user.isAdmin == true) ?
		        	<Button size="small">Edit Product</Button>
		        	:
		        	<Button size="small">Add to Cart</Button>
			    }		        
		      </CardActions>
	    	</Card>
		)


	  /*return (

			    
			    <Card className="text-center mt-2" >
			      <Card.Img className="mt-3" variant="top" src={`${image}`} />
			      <Card.Body>
			        <Card.Title>{name}</Card.Title>
			        <Card.Text>
			          {description}
			        </Card.Text>
			        {(user.isAdmin == true) ?
			        	<Button variant="secondary">Edit Product</Button>
			        	:
			        	<Button variant="primary">Add to Cart</Button>
			        }
			      </Card.Body>
			    </Card>

	  )*/


}