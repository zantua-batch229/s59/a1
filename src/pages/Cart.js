import React, { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import {Card, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Login from  './ProductAdd.js'
import { withRouter } from "react-router-dom";
import Table from 'react-bootstrap/Table';




const Cart = () => {
  const [cartItems, setCartItems] = useState([]);
  const [productData, setProductData] = useState([]);
  const token = localStorage.getItem('token');
  const {user, setUser} = useContext(UserContext);
  const [totalAmount, setTotalAmount] = useState(0);
  const [totalItems, setTotalItems] = useState(0);


  useEffect(() => {
    (async () => {
      const response = await fetch(`${ process.env.REACT_APP_API_URL }/carts/${user.id}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        }
      });
      const data = await response.json();
      setCartItems(data);

      const productPromises = data.map(async (item) => {
        const productResponse = await fetch(
          `${ process.env.REACT_APP_API_URL }/products/${item.productId}`, {
              method: "GET",
              headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
              }
            }
        );
        return await productResponse.json();
      });

      setProductData(await Promise.all(productPromises));

      let amount = 0;
      let itemCount = 0;
      productData.forEach((product, index) => {
        amount += product.price * cartItems[index].quantity;
        itemCount += cartItems[index].quantity;

      });
      setTotalAmount(amount);
      setTotalItems(itemCount);

    })();
  }, []);
  const navigate = useNavigate();
  const handleCheckout = () => {
    
  fetch(`${ process.env.REACT_APP_API_URL }/carts/checkout`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        },
      })
      .then(res => res.json())
      .then(data => {

        //console.log(data);

        if(data == true){
          Swal.fire({
            title: "Successfully Checked out!",
            icon: 'Success',
          });


          navigate("/products");


        } else{
          Swal.fire({
            title: "Checkout failed!",
            icon: 'error',
            text: "Pls. try again."
          })

        }

      });

          
  };

  return (
    <>
    <Table>
      <thead>
        <tr>
          <th>Product</th>
          <th>Quantity</th>
          <th>Price</th>
          <th style={{ textAlign: "right" }}>Sub-Total</th>
        </tr>
      </thead>
      <tbody>
        {productData.map((product, index) => (
          <>
          
          <tr key={cartItems[index].id}>
            <td>{product.name}</td>
            <td>{cartItems[index].quantity}</td>
            <td>{product.price}</td>
            <td style={{ textAlign: "right" }}>{product.price * cartItems[index].quantity}</td>
          </tr>
          </>
        ))}
      </tbody>
    </Table>
    <div style={{ display: "flex", justifyContent: "space-between" }}>
      <h3>Total Items: {totalItems}</h3>
      <h3>Total Amount: {totalAmount}</h3>
    <Button variant="primary" onClick={() => handleCheckout()}>
        Checkout
    </Button>  
    </div>
    


    
    </>
  );
};

export default Cart;


