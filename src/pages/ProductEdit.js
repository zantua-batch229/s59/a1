import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext.js';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';



export default function EditProduct() {

	// State Hooks -> store values of the input fields
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [salePrice, setSalePrice] = useState('');
	const [image, setImage] = useState('');
	const [stock, setStock] = useState('');
	const [isActive, setIsActive]= useState(false);
	const [isOnSale, setIsOnSale]= useState(false);

	const token = localStorage.getItem('token');
	const itemId = localStorage.getItem('itemid');


	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();
	const [product, setProduct] = useState({
    name: name,
    price: price,
    description: description
  	});


	  useEffect(() => {
	  	console.log(`${ process.env.REACT_APP_API_URL }/products/${itemId}`);
	  	fetch(`${ process.env.REACT_APP_API_URL }/products/${itemId}`, {
	        method: "GET",
	        headers: {
	          "Content-Type": "application/json",
	          "Authorization": `Bearer ${token}`
	        }
	      })
	      .then(res => res.json())
	      .then(data => { 
	      	console.log(data);
	      	setName(data.name);
	      	setPrice(data.price);
	      	setDescription(data.description);
	      	setSalePrice(data.salePrice);
	      	setStock(data.stock);
	      	setImage(data.image);
	      	setIsActive(data.isActive);
	      	setIsOnSale(data.isOnSale);


	      	});

	    
	  }, []);

	const handleCancel = () => {
    
    	navigate("/products");
  	};


	function edit(e){
		// prevents page redirection via form submission
		e.preventDefault();
		console.log(e);


		fetch(`${ process.env.REACT_APP_API_URL }/products/${itemId}/update`, {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
          			"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price,
					salePrice: salePrice,
					image: image,
					stock: stock,
					isActive: isActive,
					isOnSale: isOnSale
				})
			})
			.then(res => res.json())
			.then(data => {

				//console.log(data);

				if(data == true){
					Swal.fire({
						title: "Successfully edited!",
						icon: 'Success',
						text: "You have successfully edited a product."
					});


					navigate("/products");


				} else{
					Swal.fire({
						title: "edit product failed!",
						icon: 'error',
						text: "Pls. try again."
					})

				}

			});

		

		
	}

	return (
		(user.isAdmin !== true) 
			?
				<Navigate to="/"/>
			:

		<Form onSubmit={(e) =>edit(e)}>
		<h1>EDIT PRODUCT</h1>
	      <Form.Group className="mb-3" id="name">
	        <Form.Label>First Name</Form.Label>
	        <Form.Control 
	        placeholder="Name" 
	        value={name} 
	        onChange={e => setName(e.target.value)} 
	        required />
	      </Form.Group>
	      <Form.Group className="mb-3" id="description">
	        <Form.Label>Description</Form.Label>
	        <Form.Control 
	        placeholder="Description" 
	        value={description} 
	        onChange={e => setDescription(e.target.value)} 
	        required />
	      </Form.Group>
	      <Form.Group className="mb-3" id="price">
	        <Form.Label>Price</Form.Label>
	        <Form.Control 
	        placeholder="price" 
	        value={price} 
	        onChange={e => setPrice(e.target.value)} 
	        required />
	      </Form.Group>
	      <Form.Group controlId='isActive'>
              <Form.Check
                type='checkbox'
                label='Is Active'
                checked={isActive}
                value={isActive}
                onChange={(e) => setIsActive(e.target.checked)}
              />
          </Form.Group>
	      <Form.Group className="mb-3" id="salePrice">
	        <Form.Label>Sale Price</Form.Label>
	        <Form.Control 
	        placeholder="salePrice" 
	        value={salePrice} 
	        onChange={e => setSalePrice(e.target.value)} 
	        required />
	      </Form.Group>
	      <Form.Group controlId='isOnSale'>
              <Form.Check
                type='checkbox'
                label='On Sale'
                checked={isOnSale}
                value={isOnSale}
                onChange={(e) => setIsOnSale(e.target.checked)}
              />
          </Form.Group>
	      <Form.Group className="mb-3" id="stock">
	        <Form.Label>Inititial Stock</Form.Label>
	        <Form.Control 
	        placeholder="stock" 
	        value={stock} 
	        onChange={e => setStock(e.target.value)} 
	        required />
	      </Form.Group>
	      
	      <Form.Group className="mb-3" id="image">
	        <Form.Label>image</Form.Label>
	        <Form.Control 
	        placeholder="image" 
	        value={image} 
	        onChange={e => setImage(e.target.value)} 
	        required />
	      </Form.Group>



      
		      	<Button variant="primary" type="submit" id="submitBtn">
		        	Save
		      	</Button> 
				<Button variant="primary" onClick={() => handleCancel()}>
				    Cancel
				</Button>

	      
	      

	      
    	</Form>
	)
}




  