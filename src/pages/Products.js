import React, { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import {Card, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Login from  './ProductAdd.js'
import { withRouter } from "react-router-dom";
import Table from 'react-bootstrap/Table';

//import axios from 'axios';

const ProductsTable = (props) => {
  const [products, setProducts] = useState([]);
  const token = localStorage.getItem('token');
  const {user} = useContext(UserContext);
  const navigate = useNavigate();





  useEffect(() => {
      fetch(`${ process.env.REACT_APP_API_URL }/products/allproducts`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => { setProducts(data) });
  }, []);

  const handleEdit = (product) => {
    localStorage.setItem('itemid', product);
    navigate("/productEdit");
  };
  const handleAdd = () => {
    
    navigate("/productAdd");
  };

  const handleDelete = (productId) => {
    if(user.id !== null){
      fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}/archive`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {



        if(data == true){
          Swal.fire({
            title: "Item Archived!",
            icon: 'Success',
          });




        } else{
          Swal.fire({
            title: `${data}`,
            icon: 'error',
            text: "Pls. try again."
          })

        }

      });
    }

  
  };



  return (
    (user.isAdmin !== true) 
      ?
        <Navigate to="/"/>
      :
    <>
      <Button className="mt-3" variant="primary" onClick={() => handleAdd()}>
        Add Product
      </Button>

      <Table striped responsive="sm">
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Sale Price</th>
            <th>Active</th>
            <th>On Sale</th>
            <th>Stock</th>
            <th>Image URL</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {products.map(product => (
            <tr key={product._id}>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>{product.price}</td>
              <td>{product.salePrice}</td>
              <td>{product.isActive.toString()}</td>
              <td>{product.isOnSale.toString()}</td>
              <td>{product.stock}</td>
              <td>{product.image}</td>
              
              <td>
                <Button onClick={() => handleEdit(product._id)}>Edit</Button>
                <Button onClick={() => handleDelete(product._id)}>Archive</Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>



    </>
  );
};

export default ProductsTable;
/*import React, { useState, useEffect } from 'react';
import { Card, Button, Table, Form, Modal, Label, Input } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Navigate } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import addProducts from  './ProductAdd.js'

const ProductsTable = () => {
  const [products, setProducts] = useState([]);
  const [editingProduct, setEditingProduct] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);


  useEffect(() => {
  		console.log("ASDf")
        fetch(`${process.env.REACT_APP_API_URL}/products`)
          .then(res => res.json())
          .then(data => { setProducts(data) });
  }, []);

  const handleEdit = (product) => {
    setModalOpen(true);
    
  };

  const handleDelete = (productId) => {
    // Add code to delete the specified product from the database
  };

  const handleSave = () => {
    // Add code to save the changes to the edited product
  };

  const handleClose = () => {
    setModalOpen(false);
  };

  return (
    <>
      <Table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Active</th>
            <th>On Sale</th>
            <th>Sale Price</th>
            <th>Image URL</th>
            <th>Actions</th>

          </tr>
        </thead>

        <tbody>
          {products.map(product => (
            <tr key={product._id}>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>{product.price}</td>
              <td>{product.isActive.toString()}</td>
              <td>{product.isOnSale.toString()}</td>
              <td>{product.salePrice}</td>
              <td>{product.image}</td>
							<td>
                <Button onClick={() => handleEdit(product)}>Edit</Button>
                <Button onClick={() => handleDelete(product._id)}>Delete</Button>
              </td>
            </tr>
          ))}
        </tbody>

      </Table>

      {modalOpen && (
        <div className="modal-overlay">
          <div className="modal-content">
            <h2>Edit Product</h2>
            {editingProduct && (
              <Form>
                <label htmlFor="name">Name:</label>
                <input
                  type="text"
                  id="name"
                  value={editingProduct.name}
                  // Add code to handle changes to the name input
                />
                <label htmlFor="description">description:</label>
                <input
                  type="text"
                  id="description"
                  value={editingProduct.description}
                  // Add code to handle changes to the price input
                />
                <label htmlFor="price">Price:</label>
                <input
                  type="number"
                  id="category"
                  value={editingProduct.price}
                  // Add code to handle changes to the category input
                />
                <label htmlFor="salePrice">Sale Price:</label>
                <input
                  type="number"
                  id="salePrice"
                  value={editingProduct.salePrice}
                  // Add code to handle changes to the category input
                />                
								<Button onClick={handleSave}>Save</Button>
                <Button onClick={handleClose}>Cancel</Button>
              </Form>
            )}
          </div>
        </div>
      )}
      <Button variant="primary" onClick={history.push('/addProduct')}>
        Add Product
      </Button>


    </>
  );


 
};

export default ProductsTable;*/