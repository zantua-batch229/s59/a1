import Banner from  '../components/Banner.js'
import Catalogue from  './Products2.js'

const data = {
	title: "EZ Shop",
	content: "shop anywhere, anytime.",
	destination: "/products2",
	label: "View Calaogue"
}


export	default function Home(){
	return(
		<>
	      <Banner data={data}/>
	      <Catalogue/>
	      
	    </>
	);
}